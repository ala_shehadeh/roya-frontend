import React, { Component } from 'react';
import {BrowserRouter as Router, Route} from 'react-router-dom';
import Home from './Components/Home'
import 'bootstrap3/dist/css/bootstrap.min.css'
import 'font-awesome/css/font-awesome.min.css';
import './css/style.css';
import Login from './Components/login'
import Register from "./Components/register";
import AddShow from "./Components/shows/addShow";
import AllShows from "./Components/shows/allShows";
import EditShow from "./Components/shows/editShow";
import AddEpisode from "./Components/episodes/addEpisode";
import EditEpisode from "./Components/episodes/editEpisode";
import EditEpisodeForm from "./Components/episodes/editEpisodeForm";
import EpisodePage from "./Components/episodes/episodePage";
import ShowPage from "./Components/shows/showPage";

class App extends Component {
  render() {
    return (
        <Router>
                <span>
                    <Route exact path="/" component={Login}/>
                    <Route exact path="/home" component={Home}/>
                    <Route exact path="/register" component={Register} />

                    <Route exact path="/show/add" component={AddShow} />
                    <Route exact path="/show/all" component={AllShows} />
                    <Route exact path="/show/edit/:id" component={EditShow} />

                    <Route exact path="/episode/add" component={AddEpisode}/>
                    <Route exact path="/episode/edit" component={EditEpisode}/>
                    <Route exact path="/episode/edit/:id" component={EditEpisodeForm}/>
                    <Route exact path="/episode/:id" component={EpisodePage}/>
                    <Route exact path="/show/:id" component={ShowPage}/>
                </span>
        </Router>
    );
  }
}

export default App;
