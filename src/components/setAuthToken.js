import axois from 'axios'
export default function AuthToken(token)
{
    if(token) {
        axois.defaults.headers.common['Authorization'] = 'Bearer '+token;
    }
    else {
        delete axois.defaults.headers.common['Authorization'];
    }
}