import React, {Component} from 'react';
import axios from 'axios'
import setAuthToken from './setAuthToken'
import ImageLoader from "./loader";
import { Redirect,Link } from 'react-router-dom'

class Login extends Component {
    constructor() {
    super();
    this.state = {
        loader: null,
        redirect: false
    };
    this.userLogin.bind(this);
}
    userLogin(e) {
        var $this = this;
        e.preventDefault();

        //show loader
        this.setState({
            loader: <ImageLoader/>
        });
        axios.post(global.host+'login', {
            address: $this.refs.username.value,
            pwd: $this.refs.pwd.value
        }).then(function (res) {
            setAuthToken(res.data.token);

            localStorage.setItem('token', res.data.token);
            axios.get(global.host + 'users/info').then(function (xhr) {

                localStorage.setItem('user', JSON.stringify(xhr.data));
                    $this.setState({
                        redirect: true
                    })
            });
        }).catch(error => {
            $this.setState({
                loader: <div className="alert alert-danger">
                    <ul>
                        {
                            error.response.data.map((val,key)=>
                            <li>{val}</li>
                            )
                        }
                    </ul>
                </div>
            })
        });
    }

    render() {
        if(this.state.redirect)
            return ( <Redirect to='/home' />)
        else {
            return (
                <div className="container">
                    <div className="row">
                        <div className="col-md-3"></div>
                        <div className="col-md-6 col-12">
                            <div id="loginBox">
                                <form action="" method="post" onSubmit={this.userLogin.bind(this)}>
                                    <div className="form-group">
                                        <label>Email address:</label>
                                        <input type="text" className="form-control" ref="username" name="username"
                                               placeholder="Email address"/>
                                    </div>
                                    <div className="form-group">
                                        <label>Password:</label>
                                        <input type="password" className="form-control" ref="pwd" name="password"
                                               placeholder="Password"/>
                                    </div>
                                    <button type="submit" className="btn btn-info">Submit</button>
                                </form>
                                <Link to="/register">Register</Link>
                            </div>
                            {this.state.loader}
                        </div>
                        <div className="col-md-3"></div>
                    </div>
                </div>
            );
        }
        }
}

export default Login;