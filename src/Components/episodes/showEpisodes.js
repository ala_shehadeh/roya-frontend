import React,{Component} from 'react'
import axios from 'axios'
import {Link} from 'react-router-dom'

class ShowEpisodes extends Component {
    getShowInfo() {
        var $this = this
        axios.get(global.host + 'shows/selected/'+this.state.show_id)
            .then(function (xhr) {
                $this.setState({
                    episodes: xhr.data.episodes
                })
            })
    }
    deleteEpisode(id) {
        var $this = this
        axios.delete(global.host + 'admin/episodes/delete/'+id)
            .then(function () {
                $this.getShowInfo()
            })
    }
    constructor(props) {
        super()
        console.log(props)
        this.state = {
            show_id: props.id,
            episodes: []
        }
        this.getShowInfo = this.getShowInfo.bind(this)
        this.getShowInfo()
    }
    render() {
        return (
            <ul className="list-group">
                {this.state.episodes.map((value,key)=>
                    <li className="list-group-item" key={key}>
                        <div className="row">
                            <div className="col-md-10"><a href={"/episode/edit/"+value.uid}>{value.title}</a></div>
                            <div className="col-md-2" onClick={()=>this.deleteEpisode(value.uid)}><i className="fa fa-trash cursor"></i></div>
                        </div>
                    </li>
                )}
            </ul>
        )
    }
}
export default ShowEpisodes