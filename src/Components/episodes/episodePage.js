import React,{Component} from 'react'
import axios from 'axios'
import AppMenu from "../appMenu";
import ReactDOM from 'react-dom'

class EpisodePage extends Component {
    like() {
        var $this = this
        axios.put(global.host + 'users/episode/'+this.state.uid)
            .then(function () {
                $this.checkEpisodeLike();
            })
    }
    dislike(){
        var $this = this
        axios.put(global.host + 'users/dislike/episode/'+this.state.uid)
            .then(function () {
                $this.checkEpisodeLike();
            })
    }
    checkEpisodeLike() {
        var $this = this;
        axios.get(global.host + 'users/episode/'+this.state.uid)
            .then(function (xhr) {
                ReactDOM.render(
                    <button className="btn btn-danger" onClick={$this.dislike}>Dislike episode</button>
                ,document.getElementById('like'))
            })
            .catch(function (xhr) {
                if(xhr.response) {
                    ReactDOM.render(<button onClick={$this.like}  className="btn btn-info">Like episode</button>
                        ,document.getElementById('like'))
                }
            })
    }
    selectedEpisode() {
        var $this = this
        axios.get(global.host+'episodes/'+this.state.uid)
            .then(function (xhr) {
                $this.setState({
                    title: xhr.data.title,
                    thumbnail: xhr.data.thumbnail,
                    desc: xhr.data.desc,
                    key: xhr.data.youtube_key
                })
                $this.checkEpisodeLike()
            })
    }
    constructor(props) {
        super()
        this.state = {
            uid: props.match.params.id,
            title: null,
            thumbnail: null,
            desc: null,
            key: null
        }
        this.selectedEpisode = this.selectedEpisode.bind(this)
        this.checkEpisodeLike = this.checkEpisodeLike.bind(this)
        this.like = this.like.bind(this)
        this.dislike = this.dislike.bind(this)
        this.selectedEpisode()
    }
    render() {
        return (
            <div className="row">
                <div className="col-md-3" id="menuContainer">
                    <AppMenu/>
                </div>
                <div className="col-md-9">
                    <h3>{this.state.title}</h3>
                    <hr />
                    <div className="row">
                        <div className="col-md-3">
                            <img src={global.imagePath+this.state.thumbnail} className="img-thumbnail"/>
                        </div>
                        <div className="col-md-9">
                            {this.state.desc}
                            <div id="like"></div>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-md-12">
                            <iframe width="560" height="315" src={"https://www.youtube.com/embed/"+this.state.key} frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
export default EpisodePage