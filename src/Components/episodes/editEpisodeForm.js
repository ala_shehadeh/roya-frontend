import React,{Component} from 'react'
import Dropzone from 'react-dropzone'
import axios from "axios/index";
import ImageLoader from "../loader";
import AppMenu from "../appMenu";
import $ from 'jquery'

class EditEpisodeForm extends Component {
    onDrop(files) {
        var $this = this
        var formData = new FormData();
        files.forEach(file => {
            formData.append('file',file);
        });
        axios.post(global.host + 'upload',formData, {
            headers: {
                'Content-Type': 'multipart/form-data'
            }
        }).then(function (xhr) {
            if(xhr.status == 200) {
                $this.setState({
                    thumbnail: xhr.data
                })
            }
        });
    }
    constructor(props) {
        super()
        console.log(props.match)
        this.state = {
            id: props.match.params.id,
            title: null,
            duration: null,
            time: null,
            thumbnail: null,
            descripition: null,
            url: null
        }
        this.selectedEpisode = this.selectedEpisode.bind(this)
        this.selectedEpisode()
    }
    selectedEpisode() {
        var $this = this
        axios.get(global.host + 'episodes/'+this.state.id)
            .then(function (xhr) {
                $this.setState({
                    title: xhr.data.title,
                    duration: xhr.data.duration,
                    time: xhr.data.time,
                    thumbnail: xhr.data.thumbnail,
                    descripition: xhr.data.desc,
                    url: xhr.data.url
                })

                $('#desc').val(xhr.data.desc)
            })
    }
    editEpisode(e) {
        var $this = this;
        e.preventDefault();

        if (this.state.thumbnail == null)
            this.setState({
                loader: <div className="alert alert-danger text-center lead">Please upload episode thumbnail</div>
            })
        else {
            //show loader
            this.setState({
                loader: <ImageLoader/>
            });
            axios.put(global.host + 'admin/episodes/edit', {
                uid: $this.state.id,
                title: $this.refs.title.value,
                duration: $this.refs.duration.value,
                descripition: $this.refs.description.value,
                thumbnail: $this.state.thumbnail.thumbnail,
                time: $this.refs.time.value,
                url: $this.refs.url.value
            }).then(function (res) {
                $this.setState({
                        loader: <div className="alert alert-success lead text-center">
                            The episode edited correctly
                        </div>
                    }
                )
            }).catch(error => {
                $this.setState({
                    loader: <div className="alert alert-danger">
                        <ul>
                            {
                                error.response.data.map((val, key) =>
                                    <li>{val}</li>
                                )
                            }
                        </ul>
                    </div>
                })
            });
        }
    }
    render() {
        return (
            <div className="row">
                <div className="col-md-3" id="menuContainer">
                    <AppMenu/>
                </div>
                <div className="col-md-9">
                    <h4>Edit Episode</h4>
                    <hr />
            <form action="" method="post" onSubmit={this.editEpisode.bind(this)}>
                <div className="form-group">
                    <label>Title:</label>
                    <input type="text" className="form-control" defaultValue={this.state.title} ref="title" name="title"
                           placeholder="Write the title"/>
                </div>

                <div className="form-group">
                    <label>Description:</label>
                    <textarea className="form-control" ref="description" name="description" id="desc"/>
                </div>

                <div className="form-group">
                    <label>Show time:</label>
                    <input type="text" className="form-control" ref="time" name="time"
                           placeholder="show time" defaultValue={this.state.time} />
                </div>

                <div className="form-group">
                    <label>Duration:</label>
                    <input type="text" className="form-control" defaultValue={this.state.duration} ref="duration" name="duration"
                           placeholder="duration"/>
                </div>

                <div className="form-group">
                    <label>URL:</label>
                    <input type="text" className="form-control" ref="url" name="url"  defaultValue={this.state.url}
                           placeholder="URL"/>
                </div>

                <div className="form-group">
                    <section>
                        <div className="dropzone">
                            <Dropzone onDrop={this.onDrop.bind(this)} accept="image/*, application/pdf">
                                <p>Try dropping some file here, or click to select file to upload.</p>
                            </Dropzone>
                        </div>
                    </section>
                </div>

                <button type="submit" className="btn btn-info">Edit Episode</button>

                {this.state.loader}
            </form>
                </div>
            </div>
        )
    }
}

export default EditEpisodeForm
