import React,{Component} from 'react'
import SelectShow from "../shows/selectShow";
import AppMenu from "../appMenu";
import AddEpisodeForm from "./addEpisodeForm";
import ReactDOM from 'react-dom'

class AddEpisode extends Component {
    selectedShow() {
        ReactDOM.render(
            <AddEpisodeForm id={document.getElementById('shows').value}/>
        ,document.getElementById('form'))
    }
    render() {
        return (
            <div className="row">
                <div className="col-md-3" id="menuContainer">
                    <AppMenu/>
                </div>
                <div className="col-md-9">
                    <h3>Add new Episode</h3>
                    <hr />
            <SelectShow change={this.selectedShow}/>
                    <div id="form"></div>
                </div>
            </div>
        )
    }
}
export default AddEpisode