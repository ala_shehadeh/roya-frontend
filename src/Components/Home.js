import React,{Component} from 'react'
import AppMenu from './appMenu'
import axios from 'axios'
import {Link} from 'react-router-dom'

class Home extends Component {
    latestEpisodes() {
        var $this = this
        axios.get(global.host + 'latest')
            .then(function (xhr) {
                $this.setState({
                    episodes: xhr.data
                })
            })
    }
    constructor() {
        super()
        this.state = {
            episodes: []
        }
        this.latestEpisodes = this.latestEpisodes.bind(this)
        this.latestEpisodes()
    }
    render() {
        return (
            <div className="row">
                <div className="col-md-3" id="menuContainer">
            <AppMenu/>
                </div>
                <div className="col-md-9">
                    <h3>Roya TV</h3>
                    <hr />
                    <div className="row">
                    {
                        this.state.episodes.map((value,key)=>
                            <div className="col-md-4">
                                <div><Link to={"/episode/"+value.uid}><img src={global.imagePath+value.thumbnail} className="img-thumbnail"/></Link></div>
                                <div className="text-center"><h4><Link to={"/episode/"+value.uid}>{value.title}</Link></h4></div>
                            </div>
                        )
                    }
                    </div>
                </div>
            </div>
        )
    }
}

export default Home