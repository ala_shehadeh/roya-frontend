import React,{Component} from 'react'
import Menu, {Item as MenuItem, SubMenu} from 'rc-menu';
import {Link} from 'react-router-dom';
import animate from 'css-animation';
import 'rc-menu/assets/index.css';
import Search from "./search";
import RandomShows from "./shows/randomShows";

class AppMenu extends Component {
    animation = {
        enter(node, done) {
            let height;
            return animate(node, 'rc-menu-collapse', {
                start() {
                    height = node.offsetHeight;
                    node.style.height = 0;
                },
                active() {
                    node.style.height = `${height}px`;
                },
                end() {
                    node.style.height = '';
                    done();
                },
            });
        },

        appear() {
            return this.enter.apply(this,arguments);
        },

        leave(node, done) {
            return animate(node, 'rc-menu-collapse', {
                start() {
                    node.style.height = `${node.offsetHeight}px`;
                },
                active() {
                    node.style.height = 0;
                },
                end() {
                    node.style.height = '';
                    done();
                },
            });
        },
    };

    commonMenu = (<Menu>
        <SubMenu title={<span>Roya shows</span>} key="1">
            <RandomShows key="1"/>
        </SubMenu>
    </Menu>);

    adminMenu = (
        <Menu>
            <SubMenu title={<span>Roya shows</span>} key="1">
                <RandomShows key="1"/>
            </SubMenu>
            <SubMenu title={<span>Roya shows mangement</span>} key="2">
                <MenuItem key="2-1"><Link to="/show/add">Add new show</Link></MenuItem>
                <MenuItem key="2-2"><Link to="/show/all">Manage shows</Link></MenuItem>
            </SubMenu>

            <SubMenu title={<span>Roya episodes mangement</span>} key="3">
                <MenuItem key="3-1"><Link to="/episode/add">Add new episode</Link></MenuItem>
                <MenuItem key="3-2"><Link to="/episode/edit">Manage episodes</Link></MenuItem>
            </SubMenu>
        </Menu>
    )

    constructor() {
        super()
        if(global.user.role == 'admin')
        this.state = {
            menu: this.adminMenu
        }
        else
            this.state = {
                menu: this.commonMenu
            }
        this.componentDidMount = this.componentDidMount.bind(this)
        this.componentDidMount()

    }

    componentDidMount() {
        this.user = JSON.parse(localStorage.getItem('user'));
        const inlineMenu = React.cloneElement(this.state.menu, {
            mode: 'inline',
            defaultOpenKeys: ['1'],
            openAnimation: this.animation,
        });
        this.setState({
            menu: inlineMenu
        })

    }
    render() {
        return (
            <div>
                    <h4 className="text-center"><Link to="/home"><img src={require("../images/logo.png")} /> </Link></h4>
                    <Search/>
                    <hr />
                    {this.state.menu}
            </div>
        )
    }
}

export default AppMenu