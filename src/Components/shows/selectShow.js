import React,{Component} from 'react'
import axios from 'axios'

class SelectShow extends Component {
    allShows() {
        var $this = this
        axios.get(global.host + 'shows/all')
            .then(function (xhr) {
                $this.setState({
                    shows: xhr.data
                })
            })
    }
    constructor(props) {
        super()
        this.state = {
            shows: [],
            change: props.change
        }
        this.allShows = this.allShows.bind(this)
        this.allShows()
    }
    render() {
        return (
            <div className="form-group">
            <select className="form-control" id="shows" onChange={this.state.change}>
                <option value="">Select show from the list</option>
                {
                    this.state.shows.map((value,key)=>
                        <option value={value.id}>{value.showName}</option>
                    )
                }
            </select>
            </div>
        )
    }
}
export default SelectShow