import React,{Component} from 'react'
import axios from 'axios'
import AppMenu from "../appMenu";
import {Link} from 'react-router-dom'


class AllShows extends Component {
    constructor() {
        super()
        this.state = {
            shows: []
        }
        this.getShows = this.getShows.bind(this)
        this.deleteShow = this.deleteShow.bind(this)
        this.getShows()
    }
    deleteShow(id) {
        var $this = this
        axios.delete(global.host + 'admin/shows/delete/'+id).then(function () {
            $this.getShows()
        })
    }
    getShows() {
        var $this = this
        axios.get(global.host+'shows/all').then(function (xhr) {
            $this.setState({
                shows: xhr.data
            })
        })
    }
    deleteShow(id) {
        var $this = this
        axios.delete(global.host+'admin/shows/delete/'+id)
            .then(function () {
                $this.getShows()
            })
    }
    render() {
        return (
            <div className="row">
                <div className="col-md-3" id="menuContainer">
                    <AppMenu/>
                </div>
                <div className="col-md-9">
                    <h3>Shows management</h3>
                    <hr />
                    <div id="shows">
                        <ul className="list-group">
                            {this.state.shows.map((value,key)=>
                                <li className="list-group-item" key={key}>
                                    <div className="row">
                                        <div className="col-md-10"><Link to={"/show/edit/"+value.id}>{value.showName}</Link></div>
                                        <div className="col-md-2" onClick={()=>this.deleteShow(value.id)}><i className="fa fa-trash cursor"></i></div>
                                    </div>
                                </li>
                            )}
                        </ul>
                    </div>
                </div>
            </div>
        )
    }
}
export default AllShows