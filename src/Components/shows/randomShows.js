import React,{Component} from 'react'
import axios from 'axios'
import {Link} from 'react-router-dom'
import {Item as MenuItem} from 'rc-menu';

class RandomShows extends Component {
    constructor(props) {
        super();
        this.state = {
            shows: [],
            key: props.key
        }
        this.getData = this.getData.bind(this)
    }
    componentDidMount() {
        this.getData()
    }
    getData() {
        var $this = this
        axios.get(global.host + 'shows/menu').then(function (xhr) {
            $this.setState({
                shows: xhr.data
            })
        })
    }
    render() {
        return (<div>
            {
                this.state.shows.map((value,key)=>
                    <MenuItem key={this.state.key + '-'+key}><Link to={"/show/"+value.id}>{value.showName}</Link></MenuItem>
                )
            }
        </div>)
    }
}
export default RandomShows