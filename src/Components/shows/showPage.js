import React,{Component} from 'react'
import axios from 'axios'
import AppMenu from "../appMenu";
import {Link} from 'react-router-dom'

class ShowPage extends Component {
    selectedShow() {
        var $this = this
        axios.get(global.host+'shows/selected/'+this.state.id)
            .then(function (xhr) {
                $this.setState({
                    showName: xhr.data.showName,
                    showDescription: xhr.data.showDescription,
                    time: xhr.data.showTime.time,
                    days: xhr.data.showTime.days,
                    episodes: xhr.data.episodes
                })
            })
    }
    constructor(props) {
        super()
        this.state = {
            id: props.match.params.id,
            showName: null,
            time: null,
            days: [],
            episodes: [],
            showDescription: null
        }
        this.selectedShow = this.selectedShow.bind(this)
        this.selectedShow()
    }
    render() {
        return (
            <div className="row">
                <div className="col-md-3" id="menuContainer">
                    <AppMenu/>
                </div>
                <div className="col-md-9">
                    <h3>{this.state.showName}</h3>
                    <hr />
                    <div className="row">
                        <div className="col-md-3">
                            <h4>Show days</h4>
                            <hr />
                                <ul className="list-group">
                                    {
                                        this.state.days.map((value, key) =>
                                            <li className="list-group-item">{value + ' @ ' + this.state.time}</li>
                                        )
                                    }
                                </ul>
                        </div>
                        <div className="col-md-9">
                            {this.state.showDescription}
                        </div>
                    </div>
                    <div className="row">
                        {
                            this.state.episodes.map((value,key)=>
                                <div className="col-md-4">
                                    <div><Link to={"/episode/"+value.uid}><img src={global.imagePath+value.thumbnail} className="img-thumbnail"/></Link></div>
                                    <div className="text-center"><h4><Link to={"/episode/"+value.uid}>{value.title}</Link></h4></div>
                                </div>
                            )
                        }
                    </div>
                </div>
            </div>
        )
    }
}
export default ShowPage