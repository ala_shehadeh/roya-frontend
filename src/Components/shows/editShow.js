import React,{Component} from 'react'
import axios from "axios/index";
import ImageLoader from "../loader";
import AppMenu from "../appMenu";
import $ from 'jquery'

class EditShow extends Component {
    selectDay(event) {
        this.setState({
            days: []
        })
        var $this = this
        $(".days:checked").map(function(){
            var value =  $(this).val()
            console.log(value)
            $this.state.days.push(value);
        });
    }
    inArray() {
        var initValues = this.state.days;
        $('#form').find(':checkbox[name^="days"]').each(function () {
                $(this).prop("checked", ($.inArray($(this).val(), initValues) != -1));
            });
    }
    getShowData(id) {
        var $this = this
        axios.get(global.host + 'shows/selected/'+this.state.id)
            .then(function (xhr) {
                $this.setState({
                    days: xhr.data.showTime.days,
                    showName: xhr.data.showName,
                    showDescription: xhr.data.showDescription,
                    time: xhr.data.showTime.time
                })
                $this.inArray();
                $('#description').val($this.state.showDescription)
            })
    }
    constructor(props) {
        super()
        this.state = {
            id: props.match.params.id,
            loader: null,
            days: [],
            showName: null,
            showDescription: null,
            time: null
        }
        this.selectDay = this.selectDay.bind(this)
        this.getShowData = this.getShowData.bind(this)
        this.inArray = this.inArray.bind(this)
        this.editShow = this.editShow.bind(this)
        this.getShowData(this.state.id)
    }
    editShow(e) {
        this.selectDay();
        var $this = this;
        e.preventDefault();

            //show loader
            this.setState({
                loader: <ImageLoader/>
            });
            axios.put(global.host + 'admin/shows/edit/'+this.state.id, {
                title: $this.refs.title.value,
                description: $this.refs.description.value,
                days: $this.state.days,
                time: $this.refs.time.value
            }).then(function (res) {
                $this.setState({
                        loader: <div className="alert alert-success lead text-center">
                            The show edited correctly
                        </div>
                    }
                )
            }).catch(error => {
                $this.setState({
                    loader: <div className="alert alert-danger">
                        <ul>
                            {
                                error.response.data.map((val, key) =>
                                    <li>{val}</li>
                                )
                            }
                        </ul>
                    </div>
                })
            });
        }
    render() {
        return (
            <div className="row">
                <div className="col-md-3" id="menuContainer">
                    <AppMenu/>
                </div>
                <div className="col-md-9">
                    <h3>Add new show</h3>
                    <hr />
                    <form id="form" action="" method="post" onSubmit={this.editShow.bind(this)}>
                        <div className="form-group">
                            <label>Show name:</label>
                            <input type="text" className="form-control" defaultValue={this.state.showName} ref="title" name="title"
                                   placeholder="Write the show name"/>
                        </div>

                        <div className="form-group">
                            <label>Show description:</label>
                            <textarea className="form-control" ref="description" name="description" id="description"/>
                        </div>

                        <div className="form-group">
                            <label>Show days</label>
                            <div onClick={this.selectDay}><label><input type="checkbox" className="form-group days" ref="days[]" name="days[]" value="sunday"/> Sunday</label></div>
                            <div onClick={this.selectDay}><label><input type="checkbox" className="form-group days" ref="days[]" name="days[]" value="monday"/> Monday</label></div>
                            <div onClick={this.selectDay}><label><input type="checkbox" className="form-group days" ref="days[]" name="days[]" value="tuesday"/> Tuesday</label></div>
                            <div onClick={this.selectDay}><label><input type="checkbox" className="form-group days" ref="days[]" name="days[]" value="wednesday"/> Wednesday</label></div>
                            <div onClick={this.selectDay}><label><input type="checkbox" className="form-group days" ref="days[]" name="days[]" value="thursday"/> Thursday</label></div>
                            <div onClick={this.selectDay}><label><input type="checkbox" className="form-group days" ref="days[]" name="days[]" value="friday"/> Friday</label></div>
                            <div onClick={this.selectDay}><label><input type="checkbox" className="form-group days" ref="days[]" name="days[]" value="saturday"/> Saturday</label></div>
                        </div>

                        <div className="form-group">
                            <label>Show time:</label>
                            <input type="text" className="form-control" ref="time" name="time"
                                   placeholder="show time" defaultValue={this.state.time}/>
                        </div>
                        <button type="submit" className="btn btn-info">Add show</button>

                        {this.state.loader}
                    </form>
                </div>
            </div>
        )
    }
}

export default EditShow