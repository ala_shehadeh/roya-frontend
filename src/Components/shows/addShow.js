import React,{Component} from 'react'
import axios from "axios/index";
import ImageLoader from "../loader";
import AppMenu from "../appMenu";
import $ from 'jquery'

class AddShow extends Component {
    selectDay(event) {
        var $this = this
        $(".days:checked").map(function(){
            var value =  $(this).val()
            $this.state.days.push(value);
        });
    }
    constructor() {
        super()
        this.state = {
            loader: null,
            days: []
        }
        this.selectDay = this.selectDay.bind(this)
    }
    addShow(e) {
        var $this = this;
        e.preventDefault();

            //show loader
            this.setState({
                loader: <ImageLoader/>
            });
            axios.post(global.host + 'admin/shows/add', {
                title: $this.refs.title.value,
                description: $this.refs.description.value,
                days: $this.state.days,
                time: $this.refs.time.value
            }).then(function (res) {
                $this.setState({
                        loader: <div className="alert alert-success lead text-center">
                            The show added correctly
                        </div>
                    }
                )
            }).catch(error => {
                $this.setState({
                    loader: <div className="alert alert-danger">
                        <ul>
                            {
                                error.response.data.map((val, key) =>
                                    <li>{val}</li>
                                )
                            }
                        </ul>
                    </div>
                })
            });
        }
    render() {
        return (
            <div className="row">
                <div className="col-md-3" id="menuContainer">
                    <AppMenu/>
                </div>
                <div className="col-md-9">
                    <h3>Add new show</h3>
                    <hr />
                    <form action="" method="post" onSubmit={this.addShow.bind(this)}>
                        <div className="form-group">
                            <label>Show name:</label>
                            <input type="text" className="form-control" ref="title" name="title"
                                   placeholder="Write the show name"/>
                        </div>

                        <div className="form-group">
                            <label>Show description:</label>
                            <textarea className="form-control" ref="description" name="description"/>
                        </div>

                        <div className="form-group">
                            <label>Show days</label>
                            <div onClick={this.selectDay}><label><input type="checkbox" className="form-group days" ref="days[]" name="days[]" value="sunday"/> Sunday</label></div>
                            <div onClick={this.selectDay}><label><input type="checkbox" className="form-group days" ref="days[]" name="days[]" value="monday"/> Monday</label></div>
                            <div onClick={this.selectDay}><label><input type="checkbox" className="form-group days" ref="days[]" name="days[]" value="tuesday"/> Tuesday</label></div>
                            <div onClick={this.selectDay}><label><input type="checkbox" className="form-group days" ref="days[]" name="days[]" value="wednesday"/> Wednesday</label></div>
                            <div onClick={this.selectDay}><label><input type="checkbox" className="form-group days" ref="days[]" name="days[]" value="thursday"/> Thursday</label></div>
                            <div onClick={this.selectDay}><label><input type="checkbox" className="form-group days" ref="days[]" name="days[]" value="friday"/> Friday</label></div>
                            <div onClick={this.selectDay}><label><input type="checkbox" className="form-group days" ref="days[]" name="days[]" value="saturday"/> Saturday</label></div>
                        </div>

                        <div className="form-group">
                            <label>Show time:</label>
                            <input type="text" className="form-control" ref="time" name="time"
                                   placeholder="show time"/>
                        </div>
                        <button type="submit" className="btn btn-info">Add show</button>

                        {this.state.loader}
                    </form>
                </div>
            </div>
        )
    }
}

export default AddShow