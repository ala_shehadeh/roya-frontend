import React,{Component} from 'react'
import axios from "axios/index";
import ImageLoader from "./loader";
import { Link } from 'react-router-dom'
import Dropzone from 'react-dropzone'

class Register extends Component {
    onDrop(files) {
        var $this = this
        var formData = new FormData();
        files.forEach(file => {
            formData.append('file',file);
        });
        axios.post(global.host + 'upload',formData, {
            headers: {
                'Content-Type': 'multipart/form-data'
            }
        }).then(function (xhr) {
            if(xhr.status == 200) {
                $this.setState({
                    profile: xhr.data
                })
            }
        });
    }
    constructor() {
        super();
        this.state = {
            loader: null,
            profile: null
        };
        this.register.bind(this);
        this.onDrop = this.onDrop.bind(this)
    }
    register(e) {
        var $this = this;
        e.preventDefault();

        if (this.state.profile == null)
            this.setState({
                loader: <div className="alert alert-danger text-center lead">Please upload your profile</div>
            })
        else {
            //show loader
            this.setState({
                loader: <ImageLoader/>
            });
            axios.post(global.host + 'register', {
                email: $this.refs.username.value,
                password: $this.refs.pwd.value,
                name: $this.refs.name.value,
                profile: $this.state.profile.thumbnail
            }).then(function (res) {
                $this.setState({
                        loader: <div className="alert alert-success lead text-center">
                            <Link to="/">You have registered correctly, click here to login</Link>
                        </div>
                    }
                )
            }).catch(error => {
                $this.setState({
                    loader: <div className="alert alert-danger">
                        <ul>
                            {
                                error.response.data.map((val, key) =>
                                    <li>{val}</li>
                                )
                            }
                        </ul>
                    </div>
                })
            });
        }
    }
    render() {
        return (
            <div className="container">
                <div className="row">
                    <div className="col-md-3"></div>
                    <div className="col-md-6 col-12">
                        <div id="loginBox">
                            <form action="" method="post" onSubmit={this.register.bind(this)}>
                                <div className="form-group">
                                    <label>Name:</label>
                                    <input type="text" className="form-control" ref="name" name="name"
                                           placeholder="Write your name"/>
                                </div>
                                <div className="form-group">
                                    <label>Email address:</label>
                                    <input type="text" className="form-control" ref="username" name="username"
                                           placeholder="Email address"/>
                                </div>

                                <div className="form-group">
                                    <section>
                                        <div className="dropzone">
                                            <Dropzone onDrop={this.onDrop.bind(this)} accept="image/*, application/pdf">
                                                <p>Try dropping some file here, or click to select file to upload.</p>
                                            </Dropzone>
                                        </div>
                                    </section>
                                </div>

                                <div className="form-group">
                                    <label>Password:</label>
                                    <input type="password" className="form-control" ref="pwd" name="password"
                                           placeholder="Password"/>
                                </div>
                                <button type="submit" className="btn btn-info">Register</button>
                            </form>
                            <Link to="/">Back to login page</Link>
                        </div>
                        {this.state.loader}
                    </div>
                    <div className="col-md-3"></div>
                </div>
            </div>
        )
    }
}
export default Register