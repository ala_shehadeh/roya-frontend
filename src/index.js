import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import Auth from './Components/setAuthToken'

//global variables
global.host = 'http://localhost:8000/api/v1/';
global.imagePath = 'http://localhost:8000/files/'

if(localStorage.user !== undefined) {
    global.user = JSON.parse(localStorage.user);

    //set token
    Auth(localStorage.token);
}

ReactDOM.render(<App />, document.getElementById('root'));
